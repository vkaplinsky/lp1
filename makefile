OBJECTS = pyramid.o

CC = gcc
CUNIT_PATH_PREFIX = /util/CUnit/
CUNIT_DIRECTORY = CUnit/
FLAGS = -g -c -O0 -Wall -fprofile-arcs -ftest-coverage -L


pyramid.o: pyramid.c
	$(CC) $(FLAGS) pyramid.c
 
tests: $(OBJECTS) tests.c
	$(CC) $(FLAGS) $(CUNIT_PATH_PREFIX)lib  -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) $(OBJECTS) tests.c -o tests -lcunit -lgcov

clean:
	rm -rf *~ *.o a.out tests
