#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "CUnit.h"

/* This is just a sample test function - do not use this in your real tests!
 */
void test01(void) {
  create(5);
  int expected = false;
  int actual = onTable(0);
  CU_ASSERT_EQUAL( expected , actual );
}

void test02(void) {
  create(5);
  int expected = false;
  int actual = onTable(6);
  CU_ASSERT_EQUAL( expected , actual );
}

void test03(void) {
  create(5);
  int expected = true;
  int actual = open(0);
  CU_ASSERT_EQUAL( expected , actual );
}

void test04(void) {
  create(5);
  int expected = false;
  int actual = open(6);
  CU_ASSERT_EQUAL( expected , actual );
}

void test05(void) {
  create(5);
  int expected = 0;
  int actual = on(5);
  CU_ASSERT_EQUAL( expected , actual );
}

void test06(void) {
  create(5);
  move(1,3);
  int expected = false;
  int actual = open(3);
  CU_ASSERT_EQUAL( expected , actual );
}

void test07(void) {
  create(5);
  move(1,3);
  int expected = true;
  int actual = open(1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test08(void) {
  create(5);
  move(1,3);
  int expected = false;
  int actual = onTable(1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test09(void) {
  create(5);
  move(1,3);
  int expected = true;
  int actual = onTable(3);
  CU_ASSERT_EQUAL( expected , actual );
}

void test10(void) {
  create(5);
  move(1,6);
  int expected = true;
  int actual = onTable(1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test11(void) {
  create(5);
  move(6,1);
  int expected = true;
  int actual = open(1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test11(void) {
  create(5);
  move(1,3);
  int expected = 3;
  int actual = on(1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test12(void) {
  create(5);
  move(1,3);
  int expected = true;
  int actual = above(1,3);
  CU_ASSERT_EQUAL( expected , actual );
}

void test13(void) {
  create(5);
  move(1,3);
  int expected = false;
  int actual = above(3,1);
  CU_ASSERT_EQUAL( expected , actual );
}

void test14(void) {
  create(5);
  move(1,3);
  move(2,1);
  int expected = true;
  int actual = above(2,3);
  CU_ASSERT_EQUAL( expected , actual );
}

void test15(void) {
  create(5);
  move(1,3);
  move(2,1);
  int expected = false;
  int actual = above(3,2);
  CU_ASSERT_EQUAL( expected , actual );
}

void test16(void) {
  create(3);
  move(2,3);
  move(2,1);
  int expected = true;
  int actual = open(3);
  CU_ASSERT_EQUAL(expected, actual);
}

void test17(void) {
  create(3);
  move(2,3);
  move(2,1);
  int expected = false;
  int actual = open(1);
  CU_ASSERT_EQUAL(expected, actual);
}


/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite Suite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   Suite = CU_add_suite("Suite_of_tests_with_valid_inputs", NULL, NULL);
   if (NULL == Suite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to Suite */
   if (
       //  (NULL == CU_add_test(Suite, "foo(13)", test01))  // SAMPLE!
	//(NULL == CU_add_test(Suite, "foo(13)", test02)
        //(NULL == CU_add_test(Suite, "foo(13)", test03)
        //(NULL == CU_add_test(Suite, "foo(13)", test04)
        //(NULL == CU_add_test(Suite, "foo(13)", test05)
	//(NULL == CU_add_test(Suite, "foo(13)", test06)
        //(NULL == CU_add_test(Suite, "foo(13)", test07)
        //(NULL == CU_add_test(Suite, "foo(13)", test08)
        //(NULL == CU_add_test(Suite, "foo(13)", test09)
        (NULL == CU_add_test(Suite, "open(3)", test16)
        (NULL == CU_add_test(Suite, "open(1)", test17)
       )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using automated interface, with output to 'test-Results.xml' */
   CU_set_output_filename("test");
   CU_automated_run_tests();

   CU_cleanup_registry();
   return CU_get_error();
}
